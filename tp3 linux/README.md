# Tp3 automatisation 



## Sommaire

### ➜ Cours

- [SSH](./SSH/README.md)
- [Partitionnement](./partition/README.md)

### ➜ Mémos

- [Mémo LVM](./memos/lvm.md)
- [Mémo commandes](./memos/commandes.md)
- [Mémo réseau Rocky Linux](./memos/rocky_network.md)





## 

le nom de notre script bash sera le suivant tp3_automation_nextcloud.sh