# TP2 : Ethernet, IP, et ARP



# Sommaire

- [TP2 : Ethernet, IP, et ARP](#tp2--ethernet-ip-et-arp)
- [Sommaire](#sommaire)
- [I. Setup IP](#i-setup-ip)
- [II. ARP my bro](#ii-arp-my-bro)
- [III. DHCP you too my brooo](#iii-dhcp-you-too-my-brooo)
- [IV. Avant-goût TCP et UDP](#iv-avant-goût-tcp-et-udp)


![tema le requin](./picture/wireshark.jpg)




# I. Setup IP



🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

- vous renseignerez dans le compte rendu :
  - les deux IPs choisies, en précisant le masque
  - l'adresse de réseau
  - l'adresse de broadcast
- vous renseignerez aussi les commandes utilisées pour définir les adresses IP *via* la ligne de commande

Mon IP
```
PS C:\Users\heroeselfric> ipconfig /all
[...]

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Killer E2600 Gigabit Ethernet Controller
   Adresse physique . . . . . . . . . . . : 08-8F-C3-09-C8-CB
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::a898:99e2:5b95:5642%4(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.88.129(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.192
   Passerelle par défaut. . . . . . . . . : 192.168.88.130
   IAID DHCPv6 . . . . . . . . . . . : 185110467
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-28-85-64-89-08-8F-C3-09-C8-CB
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
[...]
```

notre adresse réseaux :
```
192.168.88.128/26
```

notre adresse broadcast:
```
192.168.88.191/26
```

l'IP de Mathias 

```
mathias@MacBook-Air-de-Mathias ~ % ifconfig

    en5: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
    options=6467<RXCSUM,TXCSUM,VLAN_MTU,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
    ether 00:e0:4c:68:04:2c 
    inet6 fe80::1082:3506:9869:1dbf%en5 prefixlen 64 secured scopeid 0x17 
    inet 192.168.88.130 netmask 0xffffffc0 broadcast 192.168.88.191
    nd6 options=201<PERFORMNUD,DAD>
    media: autoselect (1000baseT <full-duplex>)
    status: active
```



🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

```
PS C:\Users\heroeselfric> ping 192.168.88.130

Envoi d’une requête 'Ping'  192.168.88.130 avec 32 octets de données :
Réponse de 192.168.88.130 : octets=32 temps<1ms TTL=64
Réponse de 192.168.88.130 : octets=32 temps<1ms TTL=64
Réponse de 192.168.88.130 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.88.130:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

🌞 **Wireshark it**


* [ICMP](./lien_wireshark/icmp.pcapng)

# II. ARP my bro


🌞 **Check the ARP table**

- utilisez une commande pour afficher votre table ARP
- déterminez la MAC de votre binome depuis votre table ARP
- déterminez la MAC de la *gateway* de votre réseau 
  - celle de votre réseau physique, WiFi, genre YNOV, car il n'y en a pas dans votre ptit LAN
  - c'est juste pour vous faire manipuler un peu encore :)

```
PS C:\Users\heroeselfric> arp -a

Interface : 192.168.88.129 --- 0x4
  Adresse Internet      Adresse physique      Type
  192.168.88.130        00-e0-4c-68-04-2c     dynamique [adresse mac de mon binome]
  192.168.88.191        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
Interface : 10.5.1.10 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.5.1.255            ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.177.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.177.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.18.88 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```


🌞 **Manipuler la table ARP**

- utilisez une commande pour vider votre table ARP
- prouvez que ça fonctionne en l'affichant et en constatant les changements
- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP


executer la commande en administrateur 
```
PS C:\Windows\system32> arp -d
```

la table arp c'est vidé

```
PS C:\Windows\system32> arp -a

Interface : 192.168.88.129 --- 0x4
  Adresse Internet      Adresse physique      Type
  192.168.88.130        00-e0-4c-68-04-2c     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.5.1.10 --- 0x7
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.177.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.33.18.88 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
```

et là on constate qu'il y a moins déléments dans la table 


Si on ping une adresse on constate que des éléments ce rajoute dans la table arp

```
PS C:\Windows\system32> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=31 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=26 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=30 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 26ms, Maximum = 31ms, Moyenne = 29ms


PS C:\Windows\system32> arp -a

Interface : 192.168.88.129 --- 0x4
  Adresse Internet      Adresse physique      Type
  192.168.88.130        00-e0-4c-68-04-2c     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.5.1.10 --- 0x7
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.177.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.18.88 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.18.194          78-4f-43-87-f5-11     dynamique
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```


🌞 **Wireshark it**

- vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
- mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois
  - déterminez, pour les deux trames, les adresses source et destination
  - déterminez à quoi correspond chacune de ces adresses

dans la première tram, l'adresse 192.168.88.129 demande a tout les autre machine présente dans le réseaux l'adresse MAC qui correspond a l'adresse IP 192.168.88.130
l'adresse source est : 192.168.88.129/26
la destination est : la Broadcast soit (192.168.88.191/26)

la deuxième tram correspond a la réponse contenant ainsi l'adresse MAC 
l'adresse source est : 
la destination est : mon adresse MAC soit (08:8f:c3:09:c8:cb)

* [ARP](./lien_wireshark/arp.pcapng)

# III. DHCP you too my brooo


🌞 **Wireshark it**

- identifiez les 4 trames DHCP lors d'un échange DHCP
  - mettez en évidence les adresses source et destination de chaque trame
- identifiez dans ces 4 trames les informations **1**, **2** et **3** dont on a parlé juste au dessus

* [DHCP](./lien_wireshark/dhcp.pcapng)

La premiere tram correspond a l'entrée du client dans le réseaux, il envois un message a tout le réseaux mais seul le serveur DHCP est censé répondre (Discover)

pour la deuxième le serveur DHCP repond au client et propose une adresse disponible pour ce connecter (Offer)

pour la troisième le client choisi la première adresse que le serveur lui a envoyé si d'autre et envoie une requete avec un identifiant, pour que le serveur DHCP sache de quel serveur le client a accepté l'adresse de l'Offer (Request)

![mignon ce chat](./picture/catshark.jpg)



# IV. Avant-goût TCP et UDP

🌞 **Wireshark it**

- déterminez à quelle IP et quel port votre PC se connecte quand vous regardez une vidéo Youtube
  - il sera sûrement plus simple de repérer le trafic Youtube en fermant tous les autres onglets et toutes les autres applications utilisant du réseau


