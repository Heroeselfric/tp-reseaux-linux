# TP1 : (re)Familiaration avec un système GNU/Linux


## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [1. Préparation de la VM](#1-préparation-de-la-vm)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine


🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

pour la machine node1.tp1.b2 
```

[eric@localhost ~]$ sudo hostname node1.tp1.b2

[eric@localhost ~]$ sudo echo "node1.tp1.b2" | sudo tee /etc/hostname node1.tp1.b2

```

pour la machine node2.tp1.b2 
```

[eric@localhost ~]$ sudo hostname node2.tp1.b2

[eric@localhost ~]$ sudo echo "node2.tp1.b2" | sudo tee /etc/hostname node2.tp1.b2

```
bien évidemment on devra redémarrer les deux machines pour que les changements ce fasse !


il faut aussi verifier l'acces a internet pour les deux Machines !

pour la machine node1.tp1.b2 
```
[eric@node1 ~]$ ping -c 4 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=24.4 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=23.4 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=24.7 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=21.3 ms

    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3006ms
    rtt min/avg/max/mdev = 21.319/23.464/24.692/1.328 ms


```

pour la machine node2.tp1.b2 
```
[eric@node2~]$ ping -c 4 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.5 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=25.1 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=24.2 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=23.6 ms

    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3005ms
    rtt min/avg/max/mdev = 23.556/25.077/27.460/1.483 ms

```

## I. Utilisateurs

[Une section dédiée aux utilisateurs est dispo dans le mémo Linux.](../../cours/memos/commandes.md#gestion-dutilisateurs).

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**, qui sera dédié à son administration

```

```


🌞 **Créer un nouveau groupe `admins`** qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.


🌞 **Ajouter votre utilisateur à ce groupe `admins`**


🌞 **Pour cela...**

🌞 **Assurez vous que la connexion SSH est fonctionnelle**, sans avoir besoin de mot de passe.


### 1. Préparation de la VM

⚠️ **Uniquement sur `node1.tp1.b2`.**

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

### 2. Partitionnement

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Utilisez LVM** pour...

🌞 **Grâce au fichier `/etc/fstab`**, faites en sorte que cette partition soit montée automatiquement au démarrage du système.


🌞 **Assurez-vous que...**

- l'unité est démarrée
- l'unitée est activée (elle se lance automatiquement au démarrage)

## 2. Création de service

![Création de service systemd](./pics/create_service.png)

### A. Unité simpliste

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Créer un fichier qui définit une unité de service** 

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**


### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**

🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**
